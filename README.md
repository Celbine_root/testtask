cd ./server 
npm install
npm run start:dev

docker-compose up -d --build

http://localhost:9999/graphql

mutation{
	signUp(username: "test_user", password:"qwerty") {
    balance
    username
    token
    id
  }
}

query{
getMe {
  id
  balance
  username
  games {
    id
    gameId
    createdAt
    game
    amount
    winAmount
  }
}
}

mutation{
	logIn(password:"qwerty", username: "test_user") {
    balance
    id
    token
    username
  }
}

subscription onBalance{
  onBalance
}

subscription onGame{
  onGame {
    amount
    game
    gameId
    id
    value
    winAmount
  	createdAt
    updatedAt
  }
}

mutation {
  betOnDice(amount: 2)
}

query{
  getGame(id: 1){
    amount
    winAmount
    game
    gameId
  }
}
