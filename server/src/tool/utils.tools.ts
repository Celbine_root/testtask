export class Utils {
  static ConvertObjectToArray(obj) {
    const result = [];
    const keys = Object.keys(obj);

    for (let i = 0, l = keys.length; i < l; i++) {
      result.push(keys[i], obj[keys[i]]);
    }
    return result;
  }

  static ToBigInt = (amount: number | string, digits: number = 1000) => Math.floor(+amount * digits);

  static FromBigInt = (amount: number | string, digits: number = 1000) => +amount / digits;

  static GetRandomInRange(min: number, max: number): number {
    return parseFloat((Number(min) + Math.random() * (Number(max) - Number(min))).toFixed(2));
  }

  static GetIntRandomInRange(min: number, max: number): number {
    return Math.floor((Number(min) + Math.random() * (Number(max) + 1 - Number(min))));
  }

  static Delay = (time: number) => new Promise((resolve) => {
    setTimeout(resolve, time);
  });

  static ToDigits(number: number | string, length: number = 2) {
    const p = 10 ** length;
    return Math.floor(Number(number) * p) / p;
  }
}
