import { AuthChecker } from 'type-graphql';
import { UserEntity } from '../database/entity';

export const customAuthChecker: AuthChecker<{ user: Partial<UserEntity> }> =
  ({
     root,
     args,
     context: {user},
     info,
   }, roles) => {
    if (!user) {
      return false;
    }

    return true;
  }
