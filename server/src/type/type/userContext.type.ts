export type UserContext = {
  user?: {
    id: number;
    username: string;
  }
  header: {[key in string]: string};
}
