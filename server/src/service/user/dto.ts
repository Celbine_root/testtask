import { ArgsType, Field, Float, ID, ObjectType } from 'type-graphql';
import { IsString, Matches, MaxLength, MinLength } from 'class-validator';
import { GameEntity } from '../../database/entity';

@ArgsType()
export class SignUpDtoInput {
  @IsString()
  @MinLength(5, {
    message: 'Минимальная длина логина 5 символов',
  })
  @MaxLength(15, {
    message: 'Максимальная длина логина 15 символов',
  })
  @Matches(/^[a-z0-9._-]+$/i, {
    message: 'Логин может состоять только из английских букв, цифр и символов: . _ -',
  })
  @Field()
  username: string;

  @IsString()
  @MinLength(5, {
    message: 'Минимальная длина пароля 5 символов',
  })
  @MaxLength(50, {
    message: 'Максимальная длина пароля 50 символов',
  })
  @Field()
  password: string;
}

@ArgsType()
export class LogInDtoInput {
  @IsString()
  @Field()
  username: string;

  @IsString()
  @Field()
  password: string;
}

@ObjectType()
export class SignUpDtoOutput {
  @Field((returns) => ID)
  id: number;

  @Field()
  username: string;

  @Field((returns) => Float)
  balance: number;

  @Field((returns) => [GameEntity])
  games: GameEntity[];

  @Field()
  token: string;
}

@ObjectType()
export class LogInDtoOutput {
  @Field((returns) => ID)
  id: number;

  @Field()
  username: string;

  @Field((returns) => Float)
  balance: number;

  @Field((returns) => [GameEntity])
  games: GameEntity[];

  @Field()
  token: string;
}

export class CreateUserDtoInput {
  constructor(
    public passwordHash: string,
    public username: string,
    public balance: number,
  ) {
  }
}
