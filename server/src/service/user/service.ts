import { CreateUserDtoInput, LogInDtoInput, LogInDtoOutput, SignUpDtoInput, SignUpDtoOutput } from './dto';
import { UserRepository } from '../../database/repository';
import { getCustomRepository } from 'typeorm';
import { compare, hash } from 'bcrypt';
import { Serializer } from '../../database/serialization/serializer';
import { sign } from 'jsonwebtoken';
import { UserContext } from '../../type';

export class UserService {
  repository: UserRepository;
  serializer: Serializer;

  constructor() {
    this.serializer = new Serializer();
    this.repository = getCustomRepository(UserRepository);
  }

  async signUp({username, password}: SignUpDtoInput) {
    const existUser = await this.repository.findOne({
      where: {
        username
      }
    });

    if (existUser) {
      throw new Error('User is already exist');
    }

    const passwordHash = await hash(password, 10);

    const newUserDto = new CreateUserDtoInput(passwordHash, username, 100);

    const newUser = await this.repository.createNewUser(newUserDto);
    const serialized = this.serializer._serializeUser(newUser);

    const token = sign({
      id: serialized.id,
      username: serialized.username
    }, process.env.JWT_SECRET, {expiresIn: 60 * 60 * 24 * 2});

    return Object.assign(new SignUpDtoOutput(), {...serialized, token});
  }

  async getMe({user}: UserContext) {
    const existUser = await this.repository.getUserById(user.id);
    console.log(existUser);
    console.log(this.serializer._serializeUser(existUser));
    return this.serializer._serializeUser(existUser);
  }

  async logIn({username, password}: LogInDtoInput) {
    const existUser = await this.repository.findOne({
      where: {
        username
      }
    });

    if (!existUser) {
      throw new Error('User is not exist');
    }

    const comparedPass = await compare(password, existUser.passwordHash);

    if (!comparedPass) {
      throw new Error('Password do not match');
    }

    const serialized = this.serializer._serializeUser(existUser);

    const token = sign({
      id: serialized.id,
      username: serialized.username
    }, process.env.JWT_SECRET, {expiresIn: 60 * 60 * 24 * 2});

    return Object.assign(new LogInDtoOutput(), {...serialized, token});
  }
}
