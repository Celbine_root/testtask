import { DiceRepository, GamesRepository, UserRepository } from '../../database/repository';
import { getCustomRepository, Transaction, TransactionRepository } from 'typeorm';
import { Serializer } from '../../database/serialization/serializer';
import { GameEnum, UserContext } from '../../type';
import { Utils } from '../../tool';
import { CreateGameDtoInput, ValueGameDtoOutput } from './dto';
import redis from '../../database/redis.connection';

export class GameService {
  repository: GamesRepository;
  serializer: Serializer;

  constructor() {
    this.serializer = new Serializer();
    this.repository = getCustomRepository(GamesRepository);
  }

  @Transaction()
  async betOnDice(
    {user}: UserContext,
    amount: number,
    @TransactionRepository() userRepository?: UserRepository,
    @TransactionRepository() diceRepository?: DiceRepository,
    @TransactionRepository() gamesRepository?: GamesRepository,
  ) {
    const existBet = await redis.get(`dice_game:${user.id}`);

    if (existBet) {
      throw new Error('Wait for the game to end');
    }

    const [existUser] = await Promise.all([userRepository.findOne({
      where: {
        username: user.username,
        id: user.id,
      },
      lock: {mode: 'pessimistic_write'},
    }),
      redis.set(`dice_game:${user.id}`, 1, 'ex', 5)
    ]);

    if (!existUser) {
      throw new Error('User does not exist');
    }

    if (existUser.balance < amount) {
      throw new Error('Not enough money');
    }

    await Promise.all([
      userRepository.decrement({id: existUser.id}, 'balance', Utils.ToBigInt(amount)),
      redis.publish('onBalance', JSON.stringify({ id: existUser.id, balance: existUser.balance - amount }))
    ]);

    const chance = Utils.GetIntRandomInRange(1, 100);

    let winAmount = 0;

    if (chance < 49) {
      winAmount = amount * 2;
      await userRepository.increment({id: existUser.id}, 'balance', Utils.ToBigInt(amount));
    }

    const newDice = await diceRepository.save({
      amount,
      winAmount,
      user,
      value: chance,
    });

    const gameDto = new CreateGameDtoInput(
      GameEnum.DICE,
      newDice.id,
      amount,
      winAmount,
      existUser,
    );

    const newGame = await gamesRepository.createGame(gameDto);

    return Object.assign(new ValueGameDtoOutput(), {...newGame, value: chance});
  }
}
