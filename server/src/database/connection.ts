import { Connection, createConnection as _createConnection } from 'typeorm';
import { DiceEntity, GameEntity, UserEntity } from './entity';

export const createConnection = (): Promise<Connection> =>
  _createConnection({
    type: 'postgres',
    synchronize: true,
    entities: [
      UserEntity,
      GameEntity,
      DiceEntity,
    ],
    host: process.env.DB_HOST,
    port: +process.env.DB_PORT,
    password: process.env.DB_PASSWORD,
    username: process.env.DB_USERNAME,
    database: process.env.DB_NAME,
  });
