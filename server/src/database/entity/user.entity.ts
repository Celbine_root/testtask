import { CustomBaseEntity } from './base.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { Utils } from '../../tool';
import { GameEntity } from './game.entity';
import { Field, Float, ObjectType } from 'type-graphql';

@ObjectType()
@Entity('user')
export class UserEntity extends CustomBaseEntity {
  @Field((type) => Float)
  @Column('bigint', {
    default: 0,
    transformer: {
      to: Utils.ToBigInt,
      from: Utils.FromBigInt,
    },
    nullable: false
  })
  balance: number;

  @Column('character varying', {name: "password_hash"})
  passwordHash: string;

  @Field()
  @Column('character varying')
  username: string;

  @Field((type) => [GameEntity])
  @OneToMany(type => GameEntity, (game) => game.user)
  games: GameEntity[];
}
