import { CustomBaseEntity } from './base.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Utils } from '../../tool';
import { UserEntity } from './user.entity';

@Entity('dice_game')
export class DiceEntity extends CustomBaseEntity {
  @Column('bigint', {
    default: 0,
    transformer: {
      to: Utils.ToBigInt,
      from: Utils.FromBigInt,
    },
    nullable: false
  })
  amount: number;

  @Column('bigint', {
    default: 0,
    name: 'win_amount',
    transformer: {
      to: Utils.ToBigInt,
      from: Utils.FromBigInt,
    },
    nullable: false
  })
  winAmount: number;

  @Column('integer')
  value: number;

  @ManyToOne((type) => UserEntity)
  @JoinColumn({
    name: 'user_id',
    referencedColumnName: 'id',
  })
  user: UserEntity;
}
