import { BaseEntity, Column, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';

@ObjectType()
export class CustomBaseEntity extends BaseEntity {
  @Field((type) => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column('timestamp without time zone', {
    nullable: false,
    name: 'created_at',
    default: () => '(now() at time zone \'Europe/Moscow\')',
  })
  createdAt: string;

  @Field({ nullable: true })
  @UpdateDateColumn({name: 'updated_at', nullable: true})
  updatedAt?: string;
}
